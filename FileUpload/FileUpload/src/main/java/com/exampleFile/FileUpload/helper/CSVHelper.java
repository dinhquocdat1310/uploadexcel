/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exampleFile.FileUpload.helper;

import com.exampleFile.FileUpload.model.DracoXlsx;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Asus
 */
public class CSVHelper {

    public static String TYPE = "application/vnd.ms-excel";    
    static String[] HEADERs = {"keyNumber", "summary", "status", "statusHS", "numOfEmp", "nameOfEmp", "product", "nameOfCus", "phoneNumber", "groupOfCus", "location"};
      
    
    public static boolean hasCSVFormat(MultipartFile file) {      
        if (!TYPE.equals(file.getContentType())) {             
            return false;
        }              
        return true;
    }

    public static List<DracoXlsx> csvToTutorials(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                CSVParser csvParser = new CSVParser(fileReader,
                        CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            List<DracoXlsx> dracoXlsxs = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                DracoXlsx dracoXlsx = new DracoXlsx(
                        csvRecord.get("keyNumber"), 
                        csvRecord.get("summary"), 
                        csvRecord.get("status"), 
                        csvRecord.get("statusHS"), 
                        csvRecord.get("numOfEmp"), 
                        csvRecord.get("nameOfEmp"), 
                        csvRecord.get("product"), 
                        csvRecord.get("nameOfCus"), 
                        Integer.parseInt(csvRecord.get("phoneNumber")), 
                        csvRecord.get("groupOfCus"), 
                        csvRecord.get("location"));
                dracoXlsxs.add(dracoXlsx);
            }
            return dracoXlsxs;
        } catch (IOException e) {
            throw new RuntimeException("Fail to parse CSV file: " + e.getMessage());
        }
    }

}
