/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exampleFile.FileUpload.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "DracoXlsx")
public class DracoXlsx implements Serializable{
  
    @Id
    @Column(name = "keyNumber", nullable = false)
    private String keyNumber;
    @Column(name = "summary")
    private String summary;
    @Column(name = "status")
    private String status;
    @Column(name = "statusHS")
    private String statusHS;
    @Column(name = "numOfEmp")
    private String numOfEmp;
    @Column(name = "nameOfEmp")
    private String nameOfEmp;
    @Column(name = "product")
    private String product;
    @Column(name = "nameOfCus")
    private String nameOfCus;
    @Column(name = "phoneNumber")
    private int phoneNumber;
    @Column(name = "groupOfCus")
    private String groupOfCus;
    @Column(name = "location")
    private String location;

    @Transient
    private MultipartFile file;

    public DracoXlsx(String keyNumber, String summary, String status, String statusHS, String numOfEmp, String nameOfEmp, String product, String nameOfCus, int phoneNumber, String groupOfCus, String location) {
        this.keyNumber = keyNumber;
        this.summary = summary;
        this.status = status;
        this.statusHS = statusHS;
        this.numOfEmp = numOfEmp;
        this.nameOfEmp = nameOfEmp;
        this.product = product;
        this.nameOfCus = nameOfCus;
        this.phoneNumber = phoneNumber;
        this.groupOfCus = groupOfCus;
        this.location = location;
    }

    public DracoXlsx() {
    }

    public String getKeyNumber() {
        return this.keyNumber;
    }

    public void setKeyNumber(String keyNumber) {
        this.keyNumber = keyNumber;
    }   

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusHS() {
        return statusHS;
    }

    public void setStatusHS(String statusHS) {
        this.statusHS = statusHS;
    }

    public String getNumOfEmp() {
        return numOfEmp;
    }

    public void setNumOfEmp(String numOfEmp) {
        this.numOfEmp = numOfEmp;
    }

    public String getNameOfEmp() {
        return nameOfEmp;
    }

    public void setNameOfEmp(String nameOfEmp) {
        this.nameOfEmp = nameOfEmp;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getNameOfCus() {
        return nameOfCus;
    }

    public void setNameOfCus(String nameOfCus) {
        this.nameOfCus = nameOfCus;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGroupOfCus() {
        return groupOfCus;
    }

    public void setGroupOfCus(String groupOfCus) {
        this.groupOfCus = groupOfCus;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "Draco {" + "keyNumber=" + keyNumber + ", summary=" + summary + ", status=" + status + ", statusHS=" + statusHS + ", numOfEmp=" + numOfEmp + ", nameOfEmp=" + nameOfEmp + ", product=" + product + ", nameOfCus=" + nameOfCus + ", phoneNumber=" + phoneNumber + ", groupOfCus=" + groupOfCus + ", location=" + location + ", file=" + file + '}';
    }

}
