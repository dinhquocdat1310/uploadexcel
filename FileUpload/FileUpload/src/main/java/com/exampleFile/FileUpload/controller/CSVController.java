/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exampleFile.FileUpload.controller;

import com.exampleFile.FileUpload.helper.CSVHelper;
import com.exampleFile.FileUpload.model.DracoXlsx;
import com.exampleFile.FileUpload.service.CSVService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@CrossOrigin("http://localhost:8081")
@Controller
@RequestMapping("/api/csv")
/**
 *
 * @author Asus
 */
public class CSVController {

    @Autowired
    CSVService fileService;

    @GetMapping(value = "/tutorials")
    public String home(Model model) {        
//        model.addAttribute("EXCELFILE", new DracoXlsx());
        List<DracoXlsx> dracoXlsxs = fileService.getAllTutorials();
        for (DracoXlsx dracoXlsx : dracoXlsxs) {            
            System.out.println(dracoXlsx.toString());
        }
        model.addAttribute("EXCELFILE", dracoXlsxs);               
        return "fromUpload";
    }

    @PostMapping("/upload")
    public String postUploadFile(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        String message = "";
        if (CSVHelper.hasCSVFormat(file)) {
            try {
                fileService.save(file);
                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                redirectAttributes.addFlashAttribute("UPLOAD_SUCCESS", message);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            System.out.println(message + " Erorr");
            redirectAttributes.addFlashAttribute("UPLOAD_FAIL", message);
        }
        return "redirect:/api/csv/tutorials";
    }

//    @GetMapping("/tutorials")
//    public String getAllTutorials(RedirectAttributes redirectAttributes) {       
//        List<DracoXlsx> dracoXlsxs = fileService.getAllTutorials();
//        System.out.println("Zooo day ne");
//        for (DracoXlsx dracoXlsx : dracoXlsxs) {
//            System.out.println(dracoXlsx.toString());
//        }
//        redirectAttributes.addFlashAttribute("EXCELFILE", dracoXlsxs);
//        return "redirect:/";
//    }

//    @GetMapping("/tutorials")
//    public ResponseEntity<List<DracoXlsx>> getAllTutorials() {
//        try {
//            List<DracoXlsx> tutorials = fileService.getAllTutorials();
//
//            if (tutorials.isEmpty()) {
//                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//            }
//
//            return new ResponseEntity<>(tutorials, HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
//    @PostMapping("/upload")
//    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
//        String message = "";                 
//        if (CSVHelper.hasCSVFormat(file)) {
//            try {                
//                fileService.save(file);                
//                message = "Uploaded the file successfully: " + file.getOriginalFilename();
//                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
//            } catch (Exception e) {
//                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
//                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
//            }
//        }
//        message = "Please upload a csv file!";
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
//    }
}
