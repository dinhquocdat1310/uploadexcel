/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exampleFile.FileUpload.repository;

import com.exampleFile.FileUpload.model.DracoXlsx;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Asus
 */

@Repository
public interface FileUploadDracoRepositoryInterface extends JpaRepository<DracoXlsx, String>{
    
    @Override
    public List<DracoXlsx> findAll();
}
