/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exampleFile.FileUpload.service;

import com.exampleFile.FileUpload.helper.CSVHelper;
import com.exampleFile.FileUpload.model.DracoXlsx;
import com.exampleFile.FileUpload.repository.FileUploadDracoRepositoryInterface;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


/**
 *
 * @author Asus
 */
@Service
public class CSVService {

    @Autowired
    FileUploadDracoRepositoryInterface repositoryInterface;

    public void save(MultipartFile file) {
        try {
            List<DracoXlsx> dracoXlsxs = CSVHelper.csvToTutorials(file.getInputStream());
            repositoryInterface.saveAll(dracoXlsxs);
        } catch (IOException e) {
            throw new RuntimeException("Fail to store csv data: " + e.getMessage());
        }
    }
        
    public List<DracoXlsx> getAllTutorials() {
        return this.repositoryInterface.findAll();
    }
}
